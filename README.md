# ZEIT ONLINE Web App (Unofficial)

Opens https://www.zeit.de/index in the Ubuntu Touch webapp container.

## License

Copyright (C) 2021  oli-ver

Licensed under the Apache Software License 2.0
